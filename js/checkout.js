/**
 * Created by Timothy on 03/02/2018.
 */
$(function() {

   //get the list of products
    getProducts();

    //initialise the add to cart button
    addToCartButton();

   //initialise the cart
   displayCart();

   //initialise clear cart function
    emptyMiniCart();

});

//collection of products
var products = {

    "item_a":{
        "name":"product a",
        "sku":"a",
        "price" : "50"
    },
    "item_b":{
        "name":"product b",
        "sku":"b",
        "price" : "30"
    },
    "item_c":{
        "name":"product c",
        "sku":"c",
        "price" : "20"
    },
    "item_d":{
        "name":"product d",
        "sku":"d",
        "price" : "15"
    }

}

//collection of offers
var offers = {
    "offer_1":{
        "sku":"a",
        "offer_qty": "3",
        "price":"130"
    },
    "offer_2":{
        "sku":"b",
        "offer_qty": "2",
        "price":"45"
    }
}

//collection of cart items
var cartItems = []

/*
 * Create the product catalogue
 */
function getProducts(){

    $.each(products,function(key,value){

        //create the product catalogue
        $('#catalog').append(`
            <div class='productRow'>
                <div class='col'>
                    <p>`+value.name+`</p>
                    <p>Price: `+value.price+`</p>
                </div>
                <div class='col'>
                    <p>Qty</p>
                    <input type="text" class="item col" id="`+value.sku+`" />
                </div>
                <div class='col addToCart' data-sku="`+value.sku+`">
                    <div class='addToCartButton'>
                        Add to Cart
                    </div>
                </div>
            </div>
        `);
    });

}

/*
 * Add to cart button
 */
function addToCartButton(){

    $('.addToCart').each(function () {

        var $this = $(this);

        $this.on("click", function () {

            var itemSku = $(this).data('sku');
            var itemQty = $('#'+itemSku).val();
            var itemName = getProductName(itemSku);

            var item = {
                "sku":itemSku,
                "name":itemName,
                "qty":itemQty
            }

            //add product to the cart
            addToCart(item);

            //empty mini cart display
            $("#cartInner").hide();
            $('#cartItems').empty();

            //update the cart total
            displayCartTotal();

        });
    });

}



/*
 *Add a selected product to the cart
 *
 */
function addToCart(item){

    var itemPrice = getPrice(item,products,offers);
    var cart = [];
    cart = {
        "sku":item.sku,
        "name":item.name,
        "qty":item.qty,
        "price":itemPrice
    }

    //search our cart items to see if this product has been added
    var cartSearch = cartItemSearch(item.sku);

    //if products is in cart delete the item and update with new values, otherwise add the item
    if (cartSearch >= 0) {

        removeCartItems(item.sku);
        cartItems.push(cart);

    } else {

        cartItems.push(cart);
    }

    return cartItems;
}

/*
 * search cart to see if product has already been added
 */
function cartItemSearch(itemSku){

   var searchResult;

   $.each( cartItems, function( key, value ) {

        if(value.sku == itemSku){

            searchResult = key;
        }

    });

    return searchResult;

}

/*
 * Remove Items from Cart
 */
function removeCartItems(itemSku){

    var index = cartItemSearch(itemSku);
    cartItems.splice(index,1);

    return cartItems;

}

/*
 * Clear the cart
 */
function clearCart(){

    delete cartItems;

}


/*
 * calculate the cart total
 */
function calCartTotal(){

    var cartTotal = 0;

    $.each( cartItems, function( key, value ) {
        cartTotal += value.price;
    });

    return cartTotal;

}

/*
 * display the cart total
 */
function displayCartTotal(){

    var getCartTotal = calCartTotal();
    $('#cartTotal').html(getCartTotal.toFixed(2));
}

/*
 * show/hide the mini cart
 */
function displayCart(){

    $('#cart').on('click',function(){

        //clear ou the cart
        $('#cartItems').html();

        //get cart items
        getCartItems();

        //display the cart
        $('#cartInner').toggle();

    })

}

/*
 * get the cart items to display
 */
function getCartItems(){

    //check we have items in the cart
    if(cartItems.length == 0){

        $("#cartItems").html("Your cart is currently empty");

    } else {

        //clear the cart to prevent duplicates
        $('#cartItems').empty();

        //add the cart items
        $.each(cartItems, function (key, value) {
            $('#cartItems').append('<div class="cartItem">' + value.name + ' x '+value.qty+' - ' + value.price + '</div>');
        });
    }

}

/*
 * Get the name of a product
 */
function getProductName(productSku){

    return products["item_"+productSku].name;

}

/*
 *Get the price of a selected product
 *
 */
function getPrice(product,products,offers){

    var itemPrice;

    //check to see if this product is on offer
    var offer = checkForOffers(product,offers);

    //get the price if on offer else cal the price
    if(offer){
        itemPrice = offer;
    }else{
        itemPrice = products["item_"+product.sku].price * product.qty;
    }

    return parseFloat(itemPrice);

}

/*
 * Check to see if a product is on offer
 */
function checkForOffers(product,offers){

    var offerPrice;

    $.each( offers, function( key, value ) {

        //if we have a match check to see if we have the qty to trigger the offer price
        if(value.sku == product.sku && value.offer_qty == product.qty){

            offerPrice = value.price;

        }

    });

    return offerPrice;

}



